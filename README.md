# Bitburner Archiver (Desktop Version)

This project is intended to be a simple tool for transferring batches of files between Bitburner
environtments and the desktop. The Bitburner script can be found [here](https://gitlab.com/talamond/bitburner-archiver).

As this is a python package it should work natively in Linux and MacOS from the terminal. In Windows
you'll need to either install python or used a pre-packaged exe under releases.

Clipboard is managed via tkinter, the windows version of python includes this automatically, other
environments may not include the relevant library automatically. It should be readily available in
Linux repositories (in Ubuntu just install python3-tk).

In later versions I plan to add fallback checks to avoid this requirement and use whatever is
available.

### Quickstart

A reminder of the available flags and options can be found by running `bar.py -h`.

By default bar uses the clipboard for storing the archive if no other option is given. This is to
make it easy to copy/paste whole folders between environments.

Simple usage:

`bar.py -c project/`

This will archive the project/ folder and all it's *valid* (Bitburner supported filetypes) contents
into the clipboard. (**NOTE:** Because of a bug in the tkinter library, the clipboard clears 
immediately upon closing, so a small window will appear that you can click to close).

After which you can run:

`bar.py -x`

Which will read the archive from the clipboard and export it's contents to the archive/ directory
(recreating the project directory as archive/project/). *This does not automatically delete
anything but will automatically overwrite files*.

If you want to add additional things to the archive after creating it, you can use:

`bar.py -a project2/`

This will add project2/ to the archive while leaving the already archived project/ untouched.

If you want to check the contents of the archive to make sure it grabbed everything, then list the
contents with:

`bar.py -l`

### Archive Files

The archiver supports using files for storing archives in addition to the clipboard.

If you want to create an archive file for easy sharing, then just add `-f file.bar` to the existing
commands, for example `bar.py -x -f file.bar`. (Put .txt on the end if you need to directly move
the file into Bitburner)

### EXE Version

An exe version is provided under [releases](https://gitlab.com/talamond/bitburner-archiver-desktop/-/releases).

This is compiled using pyInstaller with the command `pyinstaller -F --clean bar.py`
